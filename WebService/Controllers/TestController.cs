﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebService.Controllers
{

    [EnableCors(origins: "http://localhost:3642", headers: "*", methods: "*")]
    public class TestController : ApiController
    {
        
        public HttpResponseMessage Get() {
             return new HttpResponseMessage() { 
               Content=new StringContent("Get: TestMessage")
             };
        }

        public HttpResponseMessage Post()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("Post: TestMessage")
            };
        }

        [DisableCors()]
        public HttpResponseMessage Put()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("Post: TestMessage")
            };
        }
    }
}
